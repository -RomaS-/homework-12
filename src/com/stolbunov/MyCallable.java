package com.stolbunov;

import java.util.concurrent.Callable;

public class MyCallable implements Callable<IPassage> {
    private IPassage passage;

    MyCallable(IPassage passage) {
        this.passage = passage;
    }

    @Override
    public IPassage call() throws Exception {
        passage.startWay();
        return passage;
    }
}
