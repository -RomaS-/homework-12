package com.stolbunov;

import com.stolbunov.dangeons.passage_of_labyrinths.IMazeRunner;

public interface IMazeRunnerFactory {
    IMazeRunner create();
}
