package com.stolbunov.partisipants.maze_runners;

import com.stolbunov.IMazeRunnerFactory;
import com.stolbunov.dangeons.passage_of_labyrinths.IMazeRunner;

public class MazeRunnerFactory implements IMazeRunnerFactory {
    private int initialCoordinateY = 0;
    private int initialCoordinateX = 0;

    @Override
    public IMazeRunner create() {
        return new MazeRunner(initialCoordinateY, initialCoordinateX);
    }
}
