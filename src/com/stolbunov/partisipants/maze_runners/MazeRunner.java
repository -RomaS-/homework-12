package com.stolbunov.partisipants.maze_runners;

import com.stolbunov.dangeons.passage_of_labyrinths.IMazeRunner;

public class MazeRunner implements IMazeRunner {
    private int initialCoordinateY;
    private int initialCoordinateX;

    MazeRunner(int initialCoordinateY, int initialCoordinateX) {
        this.initialCoordinateY = initialCoordinateY;
        this.initialCoordinateX = initialCoordinateX;
    }

    @Override
    public int[][] goForward() {
        initialCoordinateX++;
        return new int[][]{{initialCoordinateY, initialCoordinateX}};
    }

    @Override
    public int[][] goBackward() {
        initialCoordinateX--;
        return new int[][]{{initialCoordinateY, initialCoordinateX}};
    }

    @Override
    public int[][] goRight() {
        initialCoordinateY++;
        return new int[][]{{initialCoordinateY, initialCoordinateX}};
    }

    @Override
    public int[][] goLeft() {
        initialCoordinateY--;
        return new int[][]{{initialCoordinateY, initialCoordinateX}};
    }

    @Override
    public void setCoordinateY(int coordinateY) {
        initialCoordinateY = coordinateY;
    }

    @Override
    public int getCoordinateY() {
        return initialCoordinateY;
    }

    @Override
    public void setCoordinateX(int coordinateX) {
        initialCoordinateX = coordinateX;
    }

    @Override
    public int getCoordinateX() {
        return initialCoordinateX;
    }
}
