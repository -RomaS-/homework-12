package com.stolbunov;

import com.stolbunov.dangeons.passage_of_labyrinths.ILabyrinthGenerator;
import com.stolbunov.dangeons.passage_of_labyrinths.IMazeRunner;

public interface ITypePassageFactory {

    IPassage createBFS(IMazeRunner participant, ILabyrinthGenerator generator);

    IPassage createDFS(IMazeRunner participant, ILabyrinthGenerator generator);
}
