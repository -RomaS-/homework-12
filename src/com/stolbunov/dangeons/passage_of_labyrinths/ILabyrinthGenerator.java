package com.stolbunov.dangeons.passage_of_labyrinths;

public interface ILabyrinthGenerator {
    char iconEmptySquare = '\u2610';
    char iconLabeledSquare = '\u2612';
    char iconFinish = '\u2FA8';
    char iconBarrier = '\u2617';
    char iconSmilingSmiley = '\u263B';

    char[][] getLabyrinth();

    int getNumberFreeCells();

    void seeMap();
}
