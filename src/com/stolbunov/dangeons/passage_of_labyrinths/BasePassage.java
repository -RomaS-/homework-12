package com.stolbunov.dangeons.passage_of_labyrinths;

import com.stolbunov.IPassage;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import static com.stolbunov.dangeons.passage_of_labyrinths.ILabyrinthGenerator.*;

public abstract class BasePassage implements IPassage {
    private final String TEXT_FOR_FINISH = "The participant successfully comes to the Finish! Not visited cells";
    private final String TEXT_ALL_PASSED = "The labyrinth passed completely but it has no way out! Not visited cells";
    protected final String TEXT_ALL_MOVE = "%s: Entire route of the participant: %s";
    protected final String TEXT_ONE_MOVE = "%s: The participant is at the mark [%d:%d] ==> %s. It remains to visit %d cells";

    protected int participantCoordinateY;
    protected int participantCoordinateX;
    protected int counterToWin;
    protected long timeWork;
    protected String name;
    private IMazeRunner participant;
    private ILabyrinthGenerator generator;
    protected Set<Cell> listTraversedCells;
    protected char[][] labyrinth;

    public BasePassage(IMazeRunner participant, ILabyrinthGenerator generator) {
        participantCoordinateY = participant.getCoordinateY();
        participantCoordinateX = participant.getCoordinateX();
        this.participant = participant;
        this.generator = generator;
        listTraversedCells = new LinkedHashSet<>();
        listTraversedCells.add(new Cell(participantCoordinateY, participantCoordinateX));
        labyrinth = generator.getLabyrinth();
        counterToWin = generator.getNumberFreeCells();
    }

    protected boolean isGameOver() {
        if (isIconInCell(participantCoordinateY, participantCoordinateX, iconFinish)
                || isIconInCell(participantCoordinateY, participantCoordinateX, iconSmilingSmiley)) {

            printMessageEndLabyrinth(TEXT_FOR_FINISH);
            return true;
        } else if ((counterToWin == 0)) {
            printMessageEndLabyrinth(TEXT_ALL_PASSED);
            return true;
        }
        return false;
    }

    protected void markWay() {
        if (isReplaceIcon()) {
            labyrinth[participantCoordinateY][participantCoordinateX] = iconLabeledSquare;
            counterToWin();
        } else if (labyrinth[participantCoordinateY][participantCoordinateX] == iconFinish) {
            counterToWin();
            labyrinth[participantCoordinateY][participantCoordinateX] = iconSmilingSmiley;
        }
    }

    private boolean isReplaceIcon() {
        return isIconInCell(participantCoordinateY, participantCoordinateX, iconEmptySquare);
    }

    protected void seeMap() {
        for (char[] lab : labyrinth) {
            System.out.println(Arrays.toString(lab));
        }
    }

    private void counterToWin() {
        counterToWin--;
    }


    private boolean isSuccessfulMovement(int[][] coordinatesMovement) {
        if (coordinatesMovement == null || coordinatesMovement[0].length < 2) {
            return false;
        } else if ((coordinatesMovement[0][0] < labyrinth.length && coordinatesMovement[0][0] >= 0)
                && (coordinatesMovement[0][1] < labyrinth[0].length && coordinatesMovement[0][1] >= 0)) {
            return !(isIconInCell(coordinatesMovement[0][0], coordinatesMovement[0][1], iconBarrier));
        } else {
            return false;
        }
    }

    protected boolean isSuccessfulMovement(int coordinateY, int coordinateX) {
        int[][] coordinates = new int[][]{{coordinateY, coordinateX}};
        return isSuccessfulMovement(coordinates);
    }

    private boolean isIconInCell(int coordinateY, int coordinateX, char icon) {
        return labyrinth[coordinateY][coordinateX] == icon;
    }

    private void printMessageEndLabyrinth(String text) {
        System.out.println(String.format(text, counterToWin));
    }

    @Override
    public long getTimeWork() {
        return timeWork;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Set<Cell> getListTraversedCells() {
        return listTraversedCells;
    }
}
