package com.stolbunov.dangeons.passage_of_labyrinths.bfs_passage;

import com.stolbunov.dangeons.passage_of_labyrinths.BasePassage;
import com.stolbunov.dangeons.passage_of_labyrinths.Cell;
import com.stolbunov.dangeons.passage_of_labyrinths.ILabyrinthGenerator;
import com.stolbunov.dangeons.passage_of_labyrinths.IMazeRunner;

import java.util.LinkedList;
import java.util.Queue;

public class BFSPassage extends BasePassage {
    private final String BFS = "BFSPassage";
    private Queue<Cell> listAvailableCell;
    private int[][] alternativeCoordinates = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};


    public BFSPassage(IMazeRunner participant, ILabyrinthGenerator generator) {
        super(participant, generator);
        listAvailableCell = new LinkedList<>();
        listAvailableCell.add(new Cell(participantCoordinateY, participantCoordinateX));
        name = BFS;
    }

    @Override
    public void startWay() throws InterruptedException {
        long startTime = System.currentTimeMillis();
        while (!isGameOver() && listAvailableCell.size() > 0) {
            Thread.sleep(100);
            movementParticipant();
            markWay();
//            seeMap();
            announcement();
        }
        long endTime = System.currentTimeMillis();
        timeWork = endTime - startTime;
//        System.out.println(String.format(TEXT_ALL_MOVE, name, listTraversedCells.toString()));
    }

    private void movementParticipant() {
        if (listAvailableCell.size() > 0) {
            Cell currentCell = listAvailableCell.poll();
            participantCoordinateY = currentCell.getY();
            participantCoordinateX = currentCell.getX();
            listTraversedCells.add(currentCell);
            searchAlternativeWays(participantCoordinateY, participantCoordinateX);
        }
    }

    private void searchAlternativeWays(int participantCoordinateY, int participantCoordinateX) {
        for (int[] alternativeCoordinate : alternativeCoordinates) {
            int newCoordinatesY = participantCoordinateY + alternativeCoordinate[0];
            int newCoordinatesX = participantCoordinateX + alternativeCoordinate[1];
            if (isSuccessfulMovement(newCoordinatesY, newCoordinatesX)) {
                Cell cell = new Cell(newCoordinatesY, newCoordinatesX);
                if (!listAvailableCell.contains(cell) && !listTraversedCells.contains(cell))
                    listAvailableCell.add(cell);
            }
        }
    }

    private void announcement() {
        Cell futureCell = listAvailableCell.peek();
        String strFutureCell = futureCell != null ? String.format("[%d:%d]", futureCell.getY(), futureCell.getX()) : "[END]";
        System.out.println(String.format(TEXT_ONE_MOVE,
                name, participantCoordinateY, participantCoordinateX, strFutureCell, counterToWin));
    }
}
