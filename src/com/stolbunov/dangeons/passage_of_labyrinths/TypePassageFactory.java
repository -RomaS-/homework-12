package com.stolbunov.dangeons.passage_of_labyrinths;

import com.stolbunov.IPassage;
import com.stolbunov.ITypePassageFactory;
import com.stolbunov.dangeons.passage_of_labyrinths.bfs_passage.BFSPassage;
import com.stolbunov.dangeons.passage_of_labyrinths.dfs_passage.DFSPassage;

public class TypePassageFactory implements ITypePassageFactory {
    @Override
    public IPassage createBFS(IMazeRunner participant, ILabyrinthGenerator generator) {
        return new BFSPassage(participant, generator);
    }

    @Override
    public IPassage createDFS(IMazeRunner participant, ILabyrinthGenerator generator) {
        return new DFSPassage(participant, generator);
    }
}
