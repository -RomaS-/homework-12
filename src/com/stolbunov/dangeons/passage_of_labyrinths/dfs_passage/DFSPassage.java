package com.stolbunov.dangeons.passage_of_labyrinths.dfs_passage;

import com.stolbunov.dangeons.passage_of_labyrinths.BasePassage;
import com.stolbunov.dangeons.passage_of_labyrinths.Cell;
import com.stolbunov.dangeons.passage_of_labyrinths.ILabyrinthGenerator;
import com.stolbunov.dangeons.passage_of_labyrinths.IMazeRunner;

import static com.stolbunov.dangeons.passage_of_labyrinths.ILabyrinthGenerator.iconLabeledSquare;
import static com.stolbunov.dangeons.passage_of_labyrinths.ILabyrinthGenerator.iconSmilingSmiley;

public class DFSPassage extends BasePassage {
    private final String DFS = "DFSPassage";
    private int[][] alternativeCoordinates = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};


    public DFSPassage(IMazeRunner participant, ILabyrinthGenerator generator) {
        super(participant, generator);
        name = DFS;
    }

    @Override
    public void startWay() throws InterruptedException {
        long startTime = System.currentTimeMillis();
        markWay();
        if (searchFinish(participantCoordinateY, participantCoordinateX)) {
            announcement("FINISH");
        } else {
            announcement("END");
        }
        long endTime = System.currentTimeMillis();
        timeWork = endTime - startTime;
//        System.out.println(String.format(TEXT_ALL_MOVE, name, listTraversedCells));
    }


    private boolean searchFinish(int newCoordinatesY, int newCoordinatesX) throws InterruptedException {
        Thread.sleep(100);
        boolean flag = false;
        if (isGameOver(newCoordinatesY, newCoordinatesX)) {
            flag = true;
        } else {
            for (int[] alternativeCoordinate : alternativeCoordinates) {
                int newY = newCoordinatesY + alternativeCoordinate[0];
                int newX = newCoordinatesX + alternativeCoordinate[1];
                if (isSuccessfulMovement(newY, newX) && !flag) {
                    listTraversedCells.add(new Cell(newY, newX));
                    announcement(newY, newX);
                    participantCoordinateY = newY;
                    participantCoordinateX = newX;
                    markWay();
//                    seeMap();
                    flag = searchFinish(participantCoordinateY, participantCoordinateX);
                }
            }
        }
        return flag;
    }

    private void announcement(String result){
        System.out.println(String.format("%s: The participant is at the mark[%d:%d] ==> [%s]. It remains to visit %d cells",
                name, participantCoordinateY, participantCoordinateX, result, counterToWin));
    }

    private void announcement(int newY, int newX) {
        String newCoordinate = String.format("%d:%d", newY, newX);
        announcement(newCoordinate);
    }

    @Override
    protected boolean isSuccessfulMovement(int coordinateY, int coordinateX) {
        return super.isSuccessfulMovement(coordinateY, coordinateX)
                && labyrinth[coordinateY][coordinateX] != iconLabeledSquare;
    }

    private boolean isGameOver(int coordinateY, int coordinateX) {
        return super.isGameOver() || labyrinth[coordinateY][coordinateX] == iconSmilingSmiley;
    }
}
