package com.stolbunov.dangeons.passage_of_labyrinths;

public interface IMazeRunner {
    int[][] goForward();

    int[][] goBackward();

    int[][] goRight();

    int[][] goLeft();

    void setCoordinateY(int coordinateY);

    int getCoordinateY();

    void setCoordinateX(int coordinateX);

    int getCoordinateX();
}
