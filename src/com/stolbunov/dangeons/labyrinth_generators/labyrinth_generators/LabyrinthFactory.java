package com.stolbunov.dangeons.labyrinth_generators.labyrinth_generators;

import com.stolbunov.ILabyrinthFactory;
import com.stolbunov.dangeons.passage_of_labyrinths.ILabyrinthGenerator;

public class LabyrinthFactory implements ILabyrinthFactory {
    private final int MAX_HEiGHT = 10;
    private final int MAX_WITH = 10;
    private final int AMOUNT_BARRIER = 10;

    @Override
    public ILabyrinthGenerator generate() {
        return new LabyrinthGenerator(MAX_HEiGHT, MAX_WITH, AMOUNT_BARRIER);
    }
}
