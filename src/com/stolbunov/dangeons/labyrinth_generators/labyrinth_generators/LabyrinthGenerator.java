package com.stolbunov.dangeons.labyrinth_generators.labyrinth_generators;

import com.stolbunov.dangeons.labyrinth_generators.BaseLabyrinthGenerator;

public class LabyrinthGenerator extends BaseLabyrinthGenerator {

    LabyrinthGenerator(int height, int with, int amountBarrier) {
        super(height, with, amountBarrier);
    }

    @Override
    protected void creatingObstacles() {
        int i = 0;
        int middleWith = with;
        while (middleWith > 0 && i < height) {
            labyrinth[i][middleWith - 1] = iconBarrier;
            i++;
            middleWith--;
        }
    }
}
