package com.stolbunov.dangeons.labyrinth_generators.random_labyrinth_generators;

import com.stolbunov.dangeons.labyrinth_generators.BaseLabyrinthGenerator;

public class RandomLabyrinthGenerator extends BaseLabyrinthGenerator {
    RandomLabyrinthGenerator(int with, int height, int amountBarrier) {
        super(with, height, amountBarrier);
    }

    @Override
    protected void creatingObstacles() {
        creatingNumberOfDecorElements(amountBarrier, iconBarrier);
    }

    private void creatingNumberOfDecorElements(int num, char icon) {
        for (int i = 0; i < num; i++) {
            creatingOneElementOfDecor(icon);
        }
    }

    private void creatingOneElementOfDecor(char icon) {
        int y = (int) (Math.random() * (height - 1) + 1);
        int x = (int) (Math.random() * (with - 1) + 1);
        if (isIconInCell(y, x, iconEmptySquare)) {
            labyrinth[y][x] = icon;
        } else {
            creatingOneElementOfDecor(icon);
        }
    }
}
