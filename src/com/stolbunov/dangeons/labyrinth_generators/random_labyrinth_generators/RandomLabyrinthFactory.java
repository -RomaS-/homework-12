package com.stolbunov.dangeons.labyrinth_generators.random_labyrinth_generators;

import com.stolbunov.ILabyrinthFactory;
import com.stolbunov.dangeons.passage_of_labyrinths.ILabyrinthGenerator;

public class RandomLabyrinthFactory implements ILabyrinthFactory {
    private final int MAX_HEiGHT = 10;
    private final int MAX_WITH = 10;
    private final int AMOUNT_BARRIER = 10;

    @Override
    public ILabyrinthGenerator generate() {
        return new RandomLabyrinthGenerator(MAX_HEiGHT, MAX_WITH, AMOUNT_BARRIER);
    }
}
