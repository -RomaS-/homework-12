package com.stolbunov.dangeons.labyrinth_generators;

import com.stolbunov.dangeons.passage_of_labyrinths.ILabyrinthGenerator;

import java.util.Arrays;

public abstract class BaseLabyrinthGenerator implements ILabyrinthGenerator {
    protected int with;
    protected int height;
    private int finishCoordinateY;
    private int finishCoordinateX;
    private int numberFreeCells;

    protected int amountBarrier;
    protected char[][] labyrinth;

    public BaseLabyrinthGenerator(int height, int with, int amountBarrier) {
        this.height = height;
        this.with = with;
        this.amountBarrier = amountBarrier;
        finishCoordinateY = height - 1;
        finishCoordinateX = with - 1;
        labyrinth = new char[height][with];
        numberFreeCells = height * with - amountBarrier;
        decorationLabyrinth();

    }

    protected abstract void creatingObstacles();

    private void decorationLabyrinth() {
        for (int i = 0; i < labyrinth.length; i++) {
            for (int j = 0; j < labyrinth[i].length; j++) {
                labyrinth[i][j] = iconEmptySquare;
            }
        }
        creatingObstacles();
        creatingFinish();
    }

    protected boolean isIconInCell(int coordinateY, int coordinateX, char icon) {
        return labyrinth[coordinateY][coordinateX] == icon;
    }

    private void creatingFinish() {
        if (dropTheCoin()) {
            labyrinth[finishCoordinateY][finishCoordinateX] = iconFinish;
        }
    }

    private boolean dropTheCoin() {
        int randomNum = (int) (Math.random() * 100);
        return (randomNum % 2) == 0;
    }

    @Override
    public char[][] getLabyrinth() {
        return labyrinth.clone();
    }

    @Override
    public int getNumberFreeCells() {
        return numberFreeCells;
    }

    @Override
    public void seeMap() {
        for (char[] lab : labyrinth) {
            System.out.println(Arrays.toString(lab));
        }
    }
}
