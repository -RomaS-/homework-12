package com.stolbunov;

import com.stolbunov.dangeons.labyrinth_generators.labyrinth_generators.LabyrinthFactory;
import com.stolbunov.dangeons.labyrinth_generators.random_labyrinth_generators.RandomLabyrinthFactory;
import com.stolbunov.dangeons.passage_of_labyrinths.ILabyrinthGenerator;
import com.stolbunov.dangeons.passage_of_labyrinths.IMazeRunner;
import com.stolbunov.dangeons.passage_of_labyrinths.TypePassageFactory;
import com.stolbunov.partisipants.maze_runners.MazeRunnerFactory;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {
    private static final String TEXT_TITLE = "%-10s%-15s%-10s";
    private static final String TEXT_LINE = "---------------------------------------------------------";
    private static final String PLACE = "Place";
    private static final String NAME = "Name";
    private static final String TIME = "Time";
    private final static int AMOUNT_THREADS = 3;
    private static ILabyrinthFactory labyrinthFactory;
    private static IMazeRunnerFactory runnerFactory;
    private static ITypePassageFactory passageFactory;
    private static IPassage passageBFS;
    private static IPassage passageDFS;
    private static ILabyrinthGenerator generator;
    private static IMazeRunner runner;
    private static SimpleDateFormat format;

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(AMOUNT_THREADS);
        format = new SimpleDateFormat("ss.SS", Locale.UK);

        labyrinthFactory = new RandomLabyrinthFactory();
        labyrinthFactory = new LabyrinthFactory();

        runnerFactory = new MazeRunnerFactory();
        passageFactory = new TypePassageFactory();
        runner = runnerFactory.create();
        generator = labyrinthFactory.generate();
        generator.seeMap();

        passageBFS = passageFactory.createBFS(runner, generator);
        passageDFS = passageFactory.createDFS(runner, generator);

        List<MyCallable> list = new ArrayList<>(AMOUNT_THREADS);
        list.add(new MyCallable(passageBFS));
        list.add(new MyCallable(passageDFS));
        counting(executor.invokeAll(list));
//        executor.shutdown();
    }

    private static void counting(List<Future<IPassage>> futures) throws InterruptedException, ExecutionException {
        LinkedList<IPassage> passages = new LinkedList<>();
        for (Future<IPassage> future : futures) {
            passages.add(future.get());
        }
        printDifferenceTime(passages);
    }

    private static long countingDifference(LinkedList<IPassage> sortPassages) {
        if (sortPassages.size() > 1) {
            IPassage passageFirst = sortPassages.getFirst();
            IPassage passageLast = sortPassages.getLast();
            return passageLast.getTimeWork() - passageFirst.getTimeWork();
        } else {
            return 0;
        }
    }

    private static void printDifferenceTime(LinkedList<IPassage> passages) {
        passages.sort(Comparator.comparingLong(IPassage::getTimeWork));
        System.out.println(String.format(TEXT_TITLE, PLACE, NAME, TIME));
        System.out.println(TEXT_LINE);
        for (int i = 0; i < passages.size(); i++) {
            String name = passages.get(i).getName();
            String time = format(passages.get(i).getTimeWork());
            System.out.println(String.format(TEXT_TITLE, i + 1, name, time));
        }
        System.out.println(TEXT_LINE);
        long difference = countingDifference(passages);
        System.out.println(String.format("The difference between the first and the last is %s", format(difference)));
        System.out.println(TEXT_LINE);
        printAllWay(passages);
    }

    private static String format(long timeMillis) {
        return format.format(timeMillis);
    }

    private static void printAllWay(LinkedList<IPassage> passages) {
        for (IPassage passage : passages) {
            System.out.println(String.format("%n%s: Entire route of the participant: %s",
                    passage.getName(), passage.getListTraversedCells().toString()));
        }
    }
}
