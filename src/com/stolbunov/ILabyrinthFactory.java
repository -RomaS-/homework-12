package com.stolbunov;

import com.stolbunov.dangeons.passage_of_labyrinths.ILabyrinthGenerator;

public interface ILabyrinthFactory {
    ILabyrinthGenerator generate();
}
