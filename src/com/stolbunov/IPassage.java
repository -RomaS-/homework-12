package com.stolbunov;

import com.stolbunov.dangeons.passage_of_labyrinths.Cell;

import java.util.Set;

public interface IPassage {
    void startWay() throws InterruptedException;

    long getTimeWork();

    String getName();

    Set<Cell> getListTraversedCells();


}
